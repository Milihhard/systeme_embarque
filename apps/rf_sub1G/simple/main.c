/****************************************************************************
 *   apps/rf_sub1G/simple/main.c
 *
 * sub1G_module support code - USB version
 *
 * Copyright 2013-2014 Nathael Pajani <nathael.pajani@ed3l.fr>
 *
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *************************************************************************** */

#include "core/system.h"
#include "core/systick.h"
#include "core/pio.h"
#include "lib/stdio.h"
#include "drivers/serial.h"
#include "drivers/gpio.h"
#include "drivers/ssp.h"
#include "drivers/i2c.h"
#include "drivers/adc.h"
#include "extdrv/cc1101.h"
#include "extdrv/status_led.h"
#include "extdrv/tmp101_temp_sensor.h"


#define MODULE_VERSION	0x02
#define MODULE_NAME "RF Sub1G - USB"


#define RF_868MHz  1
#define RF_915MHz  0
#if ((RF_868MHz) + (RF_915MHz) != 1)
#error Either RF_868MHz or RF_915MHz MUST be defined.
#endif


#define DEBUG 1
#define BUFF_LEN 60

#define SELECTED_FREQ  FREQ_SEL_48MHz

#define RF_US 35
#define RF_THEIR 25

/***************************************************************************** */
/* Pins configuration */
/* pins blocks are passed to set_pins() for pins configuration.
 * Unused pin blocks can be removed safely with the corresponding set_pins() call
 * All pins blocks may be safelly merged in a single block for single set_pins() call..
 */
const struct pio_config common_pins[] = {
	/* UART 0 */
	{ LPC_UART0_RX_PIO_0_1,  LPC_IO_DIGITAL },
	{ LPC_UART0_TX_PIO_0_2,  LPC_IO_DIGITAL },
	/* I2C 0 */
	{ LPC_I2C0_SCL_PIO_0_10, (LPC_IO_DIGITAL | LPC_IO_OPEN_DRAIN_ENABLE) },
	{ LPC_I2C0_SDA_PIO_0_11, (LPC_IO_DIGITAL | LPC_IO_OPEN_DRAIN_ENABLE) },
	/* SPI */
	{ LPC_SSP0_SCLK_PIO_0_14, LPC_IO_DIGITAL },
	{ LPC_SSP0_MOSI_PIO_0_17, LPC_IO_DIGITAL },
	{ LPC_SSP0_MISO_PIO_0_16, LPC_IO_DIGITAL },
	/* ADC */
	{ LPC_ADC_AD0_PIO_0_30, LPC_IO_ANALOG },
	{ LPC_ADC_AD1_PIO_0_31, LPC_IO_ANALOG },
	{ LPC_ADC_AD2_PIO_1_0,  LPC_IO_ANALOG },
	ARRAY_LAST_PIO,
};

const struct pio cc1101_cs_pin = LPC_GPIO_0_15;
const struct pio cc1101_miso_pin = LPC_SSP0_MISO_PIO_0_16;
const struct pio cc1101_gdo0 = LPC_GPIO_0_6;
const struct pio cc1101_gdo2 = LPC_GPIO_0_7;

#define TMP101_ADDR  0x94  /* Pin Addr0 (pin5 of tmp101) connected to VCC */
struct tmp101_sensor_config tmp101_sensor = {
	.bus_num = I2C0,
	.addr = TMP101_ADDR,
	.resolution = TMP_RES_ELEVEN_BITS,
};
const struct pio temp_alert = LPC_GPIO_0_3;

const struct pio status_led_green = LPC_GPIO_0_28;
const struct pio status_led_red = LPC_GPIO_0_29;


#define ADC_VBAT  LPC_ADC(0)
#define ADC_EXT1  LPC_ADC(1)
#define ADC_EXT2  LPC_ADC(2)

uint32_t pression = 0, humidite = 0, lumiere = 0, temperature = 0, uv = 0;
uint32_t dataPrintf;
char order[4] = "THL";
int indiceOrder = 0;


/***************************************************************************** */
void system_init()
{
	/* Stop the watchdog */
	startup_watchdog_disable(); /* Do it right now, before it gets a chance to break in */
	system_set_default_power_state();
	clock_config(SELECTED_FREQ);
	set_pins(common_pins);
	gpio_on();
	/* System tick timer MUST be configured and running in order to use the sleeping
	 * functions */
	systick_timer_on(1); /* 1ms */
	systick_start();
}

/* Define our fault handler. This one is not mandatory, the dummy fault handler
 * will be used when it's not overridden here.
 * Note : The default one does a simple infinite loop. If the watchdog is deactivated
 * the system will hang.
 */
void fault_info(const char* name, uint32_t len)
{
	uprintf(UART0, name);
	while (1);
}



/***************************************************************************** */
/* Temperature */
/* The Temperature Alert pin is on GPIO Port 0, pin 7 (PIO0_7) */
/* The I2C Temperature sensor is at address 0x94 */
void WAKEUP_Handler(void)
{
}

void temp_config()
{
	int ret = 0;

	/* Temp Alert */
	config_gpio(&temp_alert, LPC_IO_MODE_PULL_UP, GPIO_DIR_IN, 0);
	/* FIXME : add a callback on temp_alert edge */

	/* Temp sensor */
	ret = tmp101_sensor_config(&tmp101_sensor);
	if (ret != 0) {
		uprintf(UART0, "Temp config error: %d\n", ret);
	}
}

/******************************************************************************/
/* RF Communication */
#define RF_BUFF_LEN  64

static volatile int check_rx = 0;
void rf_rx_calback(uint32_t gpio)
{
	check_rx = 1;
}

static uint8_t rf_specific_settings[] = {
	CC1101_REGS(gdo_config[2]), 0x07, /* GDO_0 - Assert on CRC OK | Disable temp sensor */
	CC1101_REGS(gdo_config[0]), 0x2E, /* GDO_2 - FIXME : do something usefull with it for tests */
	CC1101_REGS(pkt_ctrl[0]), 0x0F, /* Accept all sync, CRC err auto flush, Append, Addr check and Bcast */
#if (RF_915MHz == 1)
	/* FIXME : Add here a define protected list of settings for 915MHz configuration */
#endif
};

/* RF config */
void rf_config(void)
{
	config_gpio(&cc1101_gdo0, LPC_IO_MODE_PULL_UP, GPIO_DIR_IN, 0);
	cc1101_init(0, &cc1101_cs_pin, &cc1101_miso_pin); /* ssp_num, cs_pin, miso_pin */
	/* Set default config */
	cc1101_config();
	cc1101_set_address(RF_US);
	/* And change application specific settings */
	cc1101_update_config(rf_specific_settings, sizeof(rf_specific_settings));
	set_gpio_callback(rf_rx_calback, &cc1101_gdo0, EDGE_RISING);

#ifdef DEBUG
	uprintf(UART0, "CC1101 RF link init done.\n");
#endif
}

/* Data sent on radio comes from the UART, put any data received from UART in
* cc_tx_buff and send when either '\r' or '\n' is received.
* This function is very simple and data received between cc_tx flag set and
* cc_ptr rewind to 0 may be lost. */
static volatile uint32_t cc_tx = 0;
static volatile uint8_t cc_tx_buff[RF_BUFF_LEN];
static volatile uint8_t cc_ptr = 0;
void handle_uart_cmd(uint8_t c)
{

	if ((c == '\n') || (c == '\r') || c == '\0') {
		indiceOrder = 0;
	} else {
		order[indiceOrder] = c;
		indiceOrder ++;
		if(indiceOrder >= 3){
			indiceOrder = 0;
		}
	}

	// uprintf(UART0, "%c\n\r", c);

}

void send_on_rf(void)
{
	uint8_t cc_tx_data[RF_BUFF_LEN + 2];
	uint8_t tx_len = cc_ptr;
	int ret = 0;

	/* Create a local copy */
	memcpy((char*)&(cc_tx_data[2]), (char*)cc_tx_buff, tx_len);
	/* "Free" the rx buffer as soon as possible */
	cc_ptr = 0;
	/* Prepare buffer for sending */
	cc_tx_data[0] = tx_len + 1;
	cc_tx_data[1] = RF_THEIR; /* Broadcast */
	/* Send */
	if (cc1101_tx_fifo_state() != 0) {
		cc1101_flush_tx_fifo();
	}
	ret = cc1101_send_packet(cc_tx_data, (tx_len + 2));

	#ifdef DEBUG
	uprintf(UART0, "Tx ret: %d\n", ret);
	#endif
}

void send_formatted_data_on_RF(uint8_t addr, char* buff, int size)
{
	uint8_t cc_tx_data[RF_BUFF_LEN + 2];
	uint8_t i = 0;
	// cc_tx_data[0] = RF_BUFF_LEN + 2;
	cc_tx_data[1] = addr;
	cc_tx_data[2] = 'a';
	// cc_tx_data[3] = 'j';
	// cc_tx_data[4] = '\n';
	// cc_tx_data[5] = '\0';

	while (buff[i] != '\0') {
		cc_tx_data[i + 3] = buff[i];
		i++;
	}
	cc_tx_data[i + 3] = '\0';
	cc_tx_data[0] = i + 4;

	// int y = 0;
	// while (cc_tx_data[y] != '\0') {
	// 	// uprintf(UART0, "%c\n\r", cc_tx_data[y]);
	// 	y++;
	// }
	uprintf(UART0, "%d-%d-%c\n\r", cc_tx_data[0], cc_tx_data[1], cc_tx_data[2]);
	// for(int i = 0; i < RF_BUFF_LEN - 3; i++){
	// 	cc_tx_data[i + 3] = buff[i];
	// }
	if (cc1101_tx_fifo_state() != 0) {
		cc1101_flush_tx_fifo();
	}
	cc1101_send_packet(cc_tx_data, i + 5);
	uprintf(UART0, "on est là\n\r");
	// cc1101_send_packet(cc_tx_data, RF_BUFF_LEN + 2);
}

void handle_rf_rx_data(void)
{
	uint8_t data[RF_BUFF_LEN];
	int8_t ret = 0;
	uint8_t status = 0;

	/* Check for received packet (and get it if any) */
	ret = cc1101_receive_packet(data, RF_BUFF_LEN, &status);
	/* Go back to RX mode */
	cc1101_enter_rx_mode();

#ifdef DEBUG
	// uprintf(UART0, "RF: ret:%d, st: %d.\n", ret, status);
#endif
	dataPrintf = 0;
	// uprintf(UART0, "data : %d-%d-%c-%d-%d-%d-%d\n", data[0], data[1], data[2], data[3], data[4], data[5], data[6]);
	switch (data[2]) {
		case 'p':
			dataPrintf = (data[3] << 24) | (data[4] << 16) | (data[5] << 8) | data[6];
			pression = dataPrintf;
			// uprintf(UART0, "Pression : %d hPa\n", dataPrintf);
			break;
		case 't':
			dataPrintf = (data[3] << 24) | (data[4] << 16) | (data[5] << 8) | data[6];
			temperature = dataPrintf;
			// uprintf(UART0, "Temperature : %d,%d degC\n", dataPrintf/10, dataPrintf%10);
			break;
		case 'h':
			dataPrintf = (data[3] << 8) | data[4];
			humidite = dataPrintf;
			break;
		case 'l':
			dataPrintf = (data[3] << 24) | (data[4] << 16) | (data[5] << 8) | data[6];
			lumiere = dataPrintf;
			// send_order_on_RF(RF_THEIR, 'o', 'l', 'u', 't', 'h', 'p');
			break;
		case 'u':
			dataPrintf = (data[3] << 8) | data[4];
			uv = dataPrintf;
				break;
		// manage order
		case 'o':
			uprintf(UART0, "Send an order : \n");
			break;
		default:
			uprintf(UART0, "unknown command : \n");
	}
}

int countNbDigit(int nb){
	int count = 0;
	while(nb != 0) {
		nb /= 10;
		++count;
	}

	return count;
}


//return next pos
int concat(char* buff, char* toConcat, int startSize, int toConcatSize){
	for(int i = 0; i <  toConcatSize; i++){
		buff[startSize + i] = toConcat[i];
	}
	return 1;
}

//return next pos
int concatNumber(char* buff, int toConcat, int startSize){
	int rest = 0;
	int toConcatSize = countNbDigit(toConcat);
	int i = 0;
	while(toConcat != 0) {
		rest = toConcat % 10;
		toConcat /= 10;
		buff[startSize + i] = rest + '0';
		++i;
	}
	return 1;
}

int strLen(char *str) {
	int i = 0;
	while (str[i] != '\0') {
		i++;
	}
	return i;
}

/***************************************************************************** */
int main(void)
{
	system_init();
	uart_on(UART0, 115200, handle_uart_cmd);
	ssp_master_on(0, LPC_SSP_FRAME_SPI, 8, 4*1000*1000); /* bus_num, frame_type, data_width, rate */
	i2c_on(I2C0, I2C_CLK_100KHz, I2C_MASTER);
	adc_on(NULL);
	status_led_config(&status_led_green, &status_led_red);

	/* Radio */
	rf_config();

	/* Temperature sensor */
	temp_config();

	int tempo = 0;

	while (1) {
		uint8_t status = 0;
		/* Request a Temp conversion on I2C TMP101 temperature sensor */
		tmp101_sensor_start_conversion(&tmp101_sensor); /* A conversion takes about 40ms */
		/* Start an ADC conversion to get battery voltage */
		adc_start_convertion_once(ADC_VBAT, LPC_ADC_SEQ(0), 0);

		/* Tell we are alive :) */
		msleep(200);

		/* RF */
		if (cc_tx == 1) {
			send_on_rf();
			cc_tx = 0;
		}
		/* Do not leave radio in an unknown or unwated state */
		do {
			status = (cc1101_read_status() & CC1101_STATE_MASK);
		} while (status == CC1101_STATE_TX);

		if (status != CC1101_STATE_RX) {
			static uint8_t loop = 0;
			loop++;
			if (loop > 10) {
				if (cc1101_rx_fifo_state() != 0) {
					cc1101_flush_rx_fifo();
				}
				cc1101_enter_rx_mode();
				loop = 0;
			}
		}
		if (check_rx == 1) {
			check_rx = 0;
			handle_rf_rx_data();
		}

		// char* toSend = "Pression : " + pression + " hPa\n" +
		// "Temperature : " + temperature / 10 + "," + temperature % 10 + " degC\n" +
		// "Humidite : " + humidite / 10 + "," + humidite % 10 + " rH\n" +
		// "Lumiere : " + lumiere + " lux\n" +
		// "Indice UV : " + uv + "\n";
		if(tempo%5 == 0){
			char toSend[64];

			for (int b = 0; b < 64; b++) {
				toSend[b] = '\0';
			}
			// snprintf(toSend, 64,
			// 	"%d hPa\n%d,%d degC\n%d,%d rH\n%d lux\n",
			// 	pression, temperature/10, temperature%10, humidite/10, humidite%10, lumiere);

			//char pr[10];
			//char te[10];

			//snprintf(pr, 10, "%d", pression);
			for (int z = 0; z < 3; z++) {
				switch (order[z]) {
					case 'T':
						snprintf(toSend, 64, "%s%d,%d degC\n", toSend, temperature / 10, temperature % 10);
						break;
					case 'L':
						snprintf(toSend, 64, "%s%d lux\n", toSend, lumiere);
						break;
					case 'H':
						snprintf(toSend, 64, "%s%d,%d rH\n", toSend, humidite / 10, humidite % 10);
						break;
				}
			}
			uprintf(UART0, "%s\n", toSend);
			send_formatted_data_on_RF(RF_THEIR, toSend, 100);
			uprintf(UART0, "%c-%c-%c(%d)\n\r", order[0], order[1], order[2], indiceOrder);
		}
		tempo++;
		// uprintf(UART0, "Pression : %d hPa\n", pression);
		// uprintf(UART0, "Temperature : %d,%d degC\n", temperature/10, temperature%10);
		// uprintf(UART0, "Humidite : %d,%d rH\n", humidite/10, humidite%10);
		// uprintf(UART0, "Lumiere : %d lux\n", lumiere);
		// uprintf(UART0, "Indice UV : %d\n", uv);


	}
	return 0;
}
